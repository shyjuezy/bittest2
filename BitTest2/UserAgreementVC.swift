//
//  UserAgreementVC.swift
//  BitTest2
//
//  Created by SHYJU VISWAMBARAN on 8/5/15.
//  Copyright (c) 2015 SHYJU VISWAMBARAN. All rights reserved.
//

import UIKit

class UserAgreementVC: UIViewController {

    @IBOutlet weak var backGround: UIImageView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var agreeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // User Agreement Screen Indicator is set to true, so the method viewDidAppear will be called 
        // and that method will call the MainTableViewController
        if (NSUserDefaults.standardUserDefaults().boolForKey("userAgreeInd")) {
            //TODO - Remove the println statements.
            println("UserAgreement Screen is Alreay Opened")
            return
        }
        
        self.backGround.image = UIImage(named: "backGround")
        self.logoImage.image = UIImage(named: "BullsEyeLogo")
        self.agreeButton.layer.cornerRadius = 10
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "userAgreeInd")
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        println("I am inside the Segue")
        if segue.identifier == "AgreeSegue" {
            let selectedNVC = segue.destinationViewController as? UINavigationController
            let selectedVC = selectedNVC?.topViewController as? MainTableViewController
        }
    }
    
    // Since we set the root view controller from appDelegate.swift, this section is commented out.
    /*override func viewDidAppear(animated: Bool) {
        
        if (NSUserDefaults.standardUserDefaults().boolForKey("userAgreeInd")) {
            if let resultController = storyboard!.instantiateViewControllerWithIdentifier("MainTable") as? MainTableViewController {
                presentViewController(resultController, animated: true, completion: nil)
            }
        }
        
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "userAgreeInd")
        NSUserDefaults.standardUserDefaults().synchronize()
    }*/
}
