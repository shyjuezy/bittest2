//
//  MainTableViewController.swift
//  BitTest2
//
//  Created by SHYJU VISWAMBARAN on 7/18/15.
//  Copyright (c) 2015 SHYJU VISWAMBARAN. All rights reserved.
//

import UIKit
import CoreLocation

//CLLocationManagerDelegate - This delegate uses CoreLocation frame work, so make sure the frame work is added to Build Phase.

class MainTableViewController: UITableViewController, CLLocationManagerDelegate, UITextFieldDelegate , CountryListTableVCDelegate {

    @IBOutlet weak var countryMain: UILabel!
    @IBOutlet weak var phoneNumber: UITextField!
    
    let newCountryListMain = CountryList()
    let locationManager = CLLocationManager()
    
    var firstTime:Bool = false;
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tableView.backgroundView = UIImageView(image: UIImage(named: "backGround.jpg"))
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        self.phoneNumber.delegate = self
        
        var textField1:UITextField? = nil
        
        // This section will hide the unused cells.
        self.tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if textField == self.phoneNumber
        {
            /* componentsSeparatedByCharactersInSet will create an array and remove all the non numerical character entries */
            var newString = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
            var components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
            
            var decimalString = "".join(components) as NSString
            var length = decimalString.length
            var hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
            {
                var newLength = (textField.text as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            var formattedString = NSMutableString()
            
            if hasLeadingOne
            {
                formattedString.appendString("1 ")
                index += 1
            }
            if (length - index) > 3
            {
                var areaCode = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3
            {
                var prefix = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            var remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            return false
        }
        else
        {
            return true
        }
    }
    
    
    func countrySelectDelegate (didCountrySelect: String) {
        //self.countryMain.text = didCountrySelect
        if contains(self.newCountryListMain.countryListArray, didCountrySelect) {
            println("Current Country \(self.newCountryListMain.countryListDictionary[didCountrySelect]!) \(didCountrySelect)")
            self.countryMain.text = "(\(self.newCountryListMain.countryListDictionary[didCountrySelect]!)) \(didCountrySelect)"
        }
        else {
            println("No Current Country ")
            self.countryMain.text = "- Select A Country -"
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        println("I am inside the Segue")
        if segue.identifier == "CountrySegue" {
            println("I am inside the Country Segue")
            let selectedNVC = segue.destinationViewController as? UINavigationController
            let selectedVC = selectedNVC?.topViewController as? CountryListTableVC
            
            selectedVC?.delegate = self
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)-> Void in
            
            if error != nil {
                if !self.firstTime {
                    self.countryMain.text = "- Select A Country -"
                }
                
                self.firstTime = true
                
                println("Reverse geocoder failed with error" + error.localizedDescription)
                return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as! CLPlacemark
                self.displayLocationInfo(pm)
            } else {
                println("Problem with the data received from geocoder")
                self.countryMain.font = UIFont(name: "System", size: 18)
                self.countryMain.text = "- Select A Country -"
            }
        })
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        //stop updating location to save battery life
        self.locationManager.stopUpdatingLocation()
        println(placemark.locality)
        println(placemark.postalCode)
        println(placemark.administrativeArea)
        println(placemark.country)
        println("Iso Code : " + placemark.ISOcountryCode)
        
        //TODO - Its an optional so make sure to check if the country exist in CountryList
        
        var currentCountry:String = placemark.country
        var currentIso:String = self.newCountryListMain.countryListDictionary[currentCountry]!
        println("Dialing Code : " + currentIso)
        
        //Make sure the country is in the Dictionary, if not set the table cell label 
        //to display "select a country" view
        if contains(self.newCountryListMain.countryListArray, currentCountry) {
            println("Current Country \(self.newCountryListMain.countryListDictionary[currentCountry]!) \(currentCountry)")
            self.countryMain.text = "(\(self.newCountryListMain.countryListDictionary[currentCountry]!)) \(currentCountry)"
        }
        else {
            println("No Current Country ")
            self.countryMain.text = "- Select A Country -"
        }
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Error while updating location " + error.localizedDescription)
        self.countryMain.font = UIFont(name: "Arial", size: 18)
        self.countryMain.text = "- Select A Country -"
    }

}
