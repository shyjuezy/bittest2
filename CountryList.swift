//
//  CountryList.swift
//  BitTest2
//
//  Created by SHYJU VISWAMBARAN on 7/28/15.
//  Copyright (c) 2015 SHYJU VISWAMBARAN. All rights reserved.
//

import Foundation

class CountryList {
    var countryListArray:[String] = [String]()
    var countryListDictionary:[String: String] = ["United States" : "+1", "Canada" : "+2" , "India" : "+91", "Nepal" : "+2", "China" : "+38",  "Myanmar" : "+93"]
    
    init () {
        self.countryListArray = countryListDictionary.keys.array
    }

    //Now Sort the Array List, so that the CountryListTableVC will be sorted Alphabetically.
    func getDictKeysToArray() -> [String] {
        self.countryListArray = countryListDictionary.keys.array
            
        return sorted(self.countryListArray)
    }
}
